export const Colors = Object.freeze({
    info: '#4895ec',
    error: '#ec5f59',
    success: '#67ac5b',
    warning: '#f6c244',
});

export const Spacing = Object.freeze({
    S: '4px',
    M: '8px',
    ML: '12px',
    L: '16px',
    LXL: '20px',
    XL: '24px',
    XXL: '32px',
    XXXL: '50px',
});
