import * as React from 'react';
import * as renderer from 'react-test-renderer';

import { App } from 'src/app/components';

describe('App', () => {
    it('renders without crashing', () => {
        const rendered = renderer.create(<App skipLoadingScreen/>).toJSON();
        expect(rendered).toBeTruthy();
    });
});
