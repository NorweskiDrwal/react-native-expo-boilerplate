import * as React from 'react';
import { AppLoading, Asset, Font } from 'expo';
import { Provider as StoreProvider } from 'react-redux';
import { Platform, StatusBar, View, Text } from 'react-native';

import store from 'src/app/store';

interface Props {
    skipLoadingScreen: boolean;
}

interface State {
    isLoadingComplete: boolean;
}

class App extends React.Component<Props, State> {
    public state = {
        isLoadingComplete: false,
    };
    
    private loadResourcesAsync = async () => {
        await Promise.all([
            Asset.loadAsync([]),
            Font.loadAsync({
                'space-mono': require('../assets/fonts/SpaceMono-Regular.ttf'),
                MaterialCommunityIcons: require('@expo/vector-icons/fonts/MaterialCommunityIcons.ttf'),
            }),
        ]);
    };
    private handleLoadingError = (error: Error) => console.warn(error);
    private handleFinishLoading = () => this.setState({ isLoadingComplete: true });

    public render() {
        if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
            return (
                <AppLoading
                    startAsync={this.loadResourcesAsync}
                    onError={this.handleLoadingError}
                    onFinish={this.handleFinishLoading}
                />
            );
        }

        return (
            <StoreProvider store={store}>
                {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                <Text>[Content]</Text>
            </StoreProvider>
        );
    }
}

export default App;
