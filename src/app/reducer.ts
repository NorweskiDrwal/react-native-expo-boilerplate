import { Reducer } from 'redux';

import { AppState } from './types';
import * as AppActions from './actions';

const initialState: AppState = {
    isAuthenticated: false,
};

const appReducer: Reducer<AppState> = (state = initialState, action): AppState => {
    if (AppActions.SET_AUTHENTICATION.is(action)) {
        return { isAuthenticated: true };
    }
    return state;
};

export default appReducer;
