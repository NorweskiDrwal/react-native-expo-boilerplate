export type StateType<ReducerOrMap> = ReducerOrMap extends ((...args: any[]) => any)
    ? Readonly<ReturnType<ReducerOrMap>>
    : ReducerOrMap extends object
        ? { [K in keyof ReducerOrMap]: StateType<ReducerOrMap[K]> }
        : never;
