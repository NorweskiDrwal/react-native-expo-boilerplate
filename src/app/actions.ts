import { ActionDefinition } from './types';

export const SET_AUTHENTICATION = ActionDefinition.of('SET_AUTHENTICATION');
