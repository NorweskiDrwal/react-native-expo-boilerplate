# react-native-expo-boilerplate

React Native with Expo and basic dependencies boilerplate

- redux
- redux-thunk
- firebase
- styled-components
- typescript
- jest
- material community icons

Boilerplate is configured for absolute paths and tslint
